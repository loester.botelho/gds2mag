from klayout import db
import logging
import argparse
import time
from typing import Dict, List, Tuple
import toml

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(description='Convert a GDS2 layout file into a Magic (.mag) file.')
    parser.add_argument('-c', '--config', required=True, metavar='CONF', type=str, help='TOML config file.')
    parser.add_argument('-i', '--input', required=True, metavar='GDS2', type=str, help='GDS2 file')
    parser.add_argument('-o', '--output', required=True, metavar='MAG', type=str, help='Magic output file')
    parser.add_argument('--cell', required=False, metavar='CELL', type=str, help='name of top-level cell')
    parser.add_argument('--ignore-non-rectilinear', required=False, default=False, type=bool, metavar='IGNORE',
                        help='ignore and skip parts that cannot be decomposed into rectangles')
    parser.add_argument('-v', '--verbose', action='store_true', help='show more detailed log output')

    args = parser.parse_args()

    # Setup logging
    log_level = logging.INFO
    if args.verbose:
        log_level = logging.DEBUG
    logging.basicConfig(format='%(module)16s %(levelname)8s: %(message)s', level=log_level)

    ignore_non_rectilinear = args.ignore_non_rectilinear
    gds_path = args.input
    mag_path = args.output
    config_path = args.config

    # Load config
    logger.debug("Load config file: {}".format(config_path))
    config = toml.load(config_path)

    # Load GDS2
    layout = db.Layout()
    logger.info("Reading GDS2: %s", gds_path)
    layout.read(gds_path)

    if args.cell is None:
        top_cells = layout.top_cells()
        logger.debug('Number of top cells: %d', len(top_cells))

        if len(top_cells) > 1:
            logger.warning('More than one top cells (%d). Taking the largest one. Use `--cell` to select a cell.',
                           len(top_cells))

        # Just take the largest top cell: TODO
        top_cell = max(top_cells, key=lambda cell: cell.bbox().area())
    else:
        top_cell = layout.cell(args.cell)
        if top_cell is None:
            logger.error('No such cell: `%s`', args.cell)
            exit(1)

    store_layout_to_magic_file(config, layout, top_cell, mag_path,
                               ignore_non_rectilinear=ignore_non_rectilinear,
                               gds_path=gds_path)


class Label:
    def __init__(self, layer_name: str, text_source: Tuple[int, int], polygon_source: Tuple[int, int]):
        assert isinstance(polygon_source, tuple) and len(polygon_source) == 2, "source must be a 2-tuple"
        assert isinstance(text_source, tuple) and len(polygon_source) == 2, "source must be a 2-tuple"
        self.layer_name = layer_name
        self.text_source = text_source
        self.polygon_source = polygon_source


class Layer:
    def __init__(self, layer_name: str, polygon_source: Tuple[int, int]):
        assert isinstance(polygon_source, tuple) and len(polygon_source) == 2, "source must be a 2-tuple"

        self.layer_name = layer_name
        self.polygon_source = polygon_source


def _decompose_polygon(polygon: db.Polygon, ignore_non_rectilinear: bool = False) -> List[db.Box]:
    return _decompose_region(db.Region(polygon), ignore_non_rectilinear)


def _decompose_region(region: db.Region, ignore_non_rectilinear: bool = False) -> List[db.Box]:
    """
    Decompose a `db.Region` of multiple `db.Polygon`s into non-overlapping rectangles (`db.Box`).
    :param region:
    :param ignore_non_rectilinear: If set to `True` then non-rectilinear polygons are skipped.
    :return: Returns the list of rectangles.
    """
    trapezoids = region.decompose_trapezoids_to_region()
    logger.debug("Number of trapezoids: {}".format(trapezoids.size()))
    rectangles = []
    for polygon in trapezoids.each():
        box = polygon.bbox()

        if db.Polygon(box) != polygon:
            msg = "Cannot decompose into rectangles. Something is not rectilinear!"
            if not ignore_non_rectilinear:
                logger.error(msg)
                assert False, msg
            else:
                logger.warning(msg)

        rectangles.append(box)
    return rectangles


def _format_rect(box: db.Box) -> str:
    """
    Format a rectangle as a string of the form as it is required for .mag files: "xbot ybot xtop ytop"
    :param box: The rectangle.
    :return: Returns the formatted string.
    """
    lower_left = box.p1
    upper_right = box.p2
    xbot, ybot = lower_left.x, lower_left.y
    xtop, ytop = upper_right.x, upper_right.y

    assert xbot <= xtop
    assert ybot <= ytop

    rect_str = "{} {} {} {}".format(xbot, ybot, xtop, ytop)
    return rect_str


def store_layout_to_magic_file(config: Dict,
                               layout: db.Layout,
                               top_cell: db.Cell,
                               output_file: str,
                               ignore_non_rectilinear: bool = False,
                               gds_path: str = ''):
    """
    Write the cell layout to a file in the Magic (.mag) format.

    Documentation of the magic file format: http://opencircuitdesign.com/magic/manpages/mag_manpage.html
    :param layout:
    :param top_cell:
    :param output_file:
    :param ignore_non_rectilinear:
    :param gds_path: Path of the source GDS2 file. This will be written into the comment of the .mag file.
    :return:
    """

    # Sanity checks for the configuration data structure.
    assert "tech" in config, "'tech' attribute is missing in the config file"
    assert "layers" in config and isinstance(config["layers"], dict), \
        "'layers' section is missing in the config file"
    assert "labels" in config and isinstance(config["labels"], dict), \
        "'labels' section is missing in the config file"

    layer_config = [Layer(layer_name, tuple(source_index)) for layer_name, source_index in config['layers'].items()]
    layer_mapping = {l.layer_name: l.polygon_source for l in layer_config}

    label_config = []
    for layer_name, source_index in config['labels'].items():
        text_source = tuple(source_index)

        if layer_name not in layer_mapping:
            msg = "Layer '{}' is not defined in 'layers' section!".format(layer_name)
            logger.error(msg)
            assert False, msg

        polygon_source = layer_mapping[layer_name]
        l = Label(layer_name, text_source, polygon_source)
        label_config.append(l)

    logger.info("Number of layers: {}".format(layout.layers()))
    logger.info("Processing cell: {}".format(top_cell.name))

    tech_name = config["tech"]
    mag_lines = ["magic",
                 "# Generated by gds2mag",
                 "# Source file: {}".format(gds_path),
                 "tech {}".format(tech_name),
                 "timestamp {}".format(int(time.time()))
                 ]

    mag_labels = []

    for layer in layer_config:
        text_layer_gds_index = layer.polygon_source
        idx = layout.layer(*text_layer_gds_index)
        layer_info = layout.get_info(idx)

        layer_number = layer_info.layer
        layer_datatype = layer_info.datatype
        assert text_layer_gds_index == (layer_number, layer_datatype)

        logger.debug("Processing layer {}/{} => '{}'".format(layer_number, layer_datatype, layer_name))

        # Fetch geometry of layer
        region = db.Region(top_cell.shapes(idx))

        line = "<< {} >>".format(layer.layer_name)
        mag_lines.append(line)

        # Convert region into rectangles.
        boxes = _decompose_region(region, ignore_non_rectilinear=ignore_non_rectilinear)
        # Format rectangle strings.
        rect_lines = ["rect {}".format(_format_rect(box)) for box in boxes]
        mag_lines.extend(rect_lines)

    for label in label_config:
        text_layer_gds_index = label.text_source
        text_shape = top_cell.shapes(layout.layer(*text_layer_gds_index))

        label_layer_gds_index = label.polygon_source
        label_region = db.Region(top_cell.shapes(layout.layer(*label_layer_gds_index)))

        layer_name = label.layer_name

        logger.debug("Processing label of '{}' "
                     "(text source: {}/{}, "
                     "polygon source: {}/{})".format(layer_name,
                                                     layer_number,
                                                     layer_datatype,
                                                     *label_layer_gds_index))
        # Get all db.Text objects on the text layer.
        texts = [t.text for t in text_shape.each() if t.is_text()]

        # Get all polygons on the label layer.
        label_polygons = [p for p in label_region.each()]

        # For each polygon find the text labels touching it.
        for label_polygon in label_polygons:
            # Find all labels inside the polygon.
            labels = {t.string for t in texts if label_polygon.inside(db.Point(t.x, t.y))}

            # Create Magic labels.
            if len(labels) > 0:
                rectangles = _decompose_polygon(label_polygon, ignore_non_rectilinear)
                rectangles_str = [_format_rect(r) for r in rectangles]
                for label in labels:
                    for rect_str in rectangles_str:
                        text_orientation = 0  # 0: center
                        mag_labels.append("rlabel {} {} {} {}".format(layer_name,
                                                                      rect_str,
                                                                      text_orientation,
                                                                      label))

    # for text_layer_gds_index, output_name in layer_mapping.items():
    #     idx = layout.layer(*text_layer_gds_index)
    #
    #     layer_info = layout.get_info(idx)
    #
    #     if isinstance(output_name, str):
    #         layer_name = output_name
    #     elif isinstance(output_name, Label):
    #         layer_name = output_name.layer_name
    #     else:
    #         assert False, "Output layer must be either an instance of `str` or `Label`."
    #
    #     layer_number = layer_info.layer
    #     layer_datatype = layer_info.datatype
    #     assert text_layer_gds_index == (layer_number, layer_datatype)
    #
    #     logger.debug("Processing layer {}/{} => '{}'".format(layer_number, layer_datatype, layer_name))
    #
    #     if isinstance(output_name, str):
    #         region = db.Region(top_cell.shapes(idx))
    #
    #         line = "<< {} >>".format(layer_name)
    #         mag_lines.append(line)
    #
    #         # Convert region into rectangles.
    #         boxes = _decompose_region(region, ignore_non_rectilinear=ignore_non_rectilinear)
    #         # Format rectangle strings.
    #         rect_lines = ["rect {}".format(_format_rect(box)) for box in boxes]
    #         mag_lines.extend(rect_lines)
    #
    #     elif isinstance(output_name, Label):
    #         text_shape = top_cell.shapes(idx)
    #         assert output_name.layer_name in layer_mapping_reverse
    #         label_layer_gds_index = layer_mapping_reverse[output_name.layer_name]
    #
    #         label_region = db.Region(top_cell.shapes(layout.layer(*label_layer_gds_index)))
    #
    #         logger.debug("Processing label of '{}' (text: {}/{}, shapes: {}/{})".format(layer_name,
    #                                                                                     layer_number,
    #                                                                                     layer_datatype,
    #                                                                                     label_layer_gds_index[0],
    #                                                                                     label_layer_gds_index[1]))
    #         # Get all db.Text objects on the text layer.
    #         texts = [t.text for t in text_shape.each() if t.is_text()]
    #
    #         # Get all polygons on the label layer.
    #         label_polygons = [p for p in label_region.each()]
    #
    #         # For each polygon find the text labels touching it.
    #         for label_polygon in label_polygons:
    #             # Find all labels inside the polygon.
    #             labels = {t.string for t in texts if label_polygon.inside(db.Point(t.x, t.y))}
    #
    #             # Create Magic labels.
    #             if len(labels) > 0:
    #                 rectangles = _decompose_polygon(label_polygon, ignore_non_rectilinear)
    #                 rectangles_str = [_format_rect(r) for r in rectangles]
    #                 for label in labels:
    #                     for rect_str in rectangles_str:
    #                         text_orientation = 0  # 0: center
    #                         mag_labels.append("rlabel {} {} {} {}".format(output_name.layer_name,
    #                                                                       rect_str,
    #                                                                       text_orientation,
    #                                                                       label))

    # Appends 'labels' section if there are any.
    if len(mag_labels) > 0:
        mag_lines.append("<< labels >>")
        mag_lines.extend(mag_labels)

    mag_lines.append("<< end >>\n")

    with open(output_file, "w") as mag_file:
        logger.info("Writing MAG file: {}".format(output_file))
        mag_data = "\n".join(mag_lines)
        mag_file.write(mag_data)
